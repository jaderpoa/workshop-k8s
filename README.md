# Workshop K8s

Aqui estão todos os manifestos que foram usados na apresentação dos principais objetos do Kubernetes.

Você pode clonar esse repositório e estudar usando o Kind, Minikube ou até mesmo o portal https://labs.play-with-k8s.com/

Para uma imersão mais completa, sugere-se o uso do serviço https://www.oracle.com/cloud/free/

